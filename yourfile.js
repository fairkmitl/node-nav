fetchData(process.argv[2]);

const fund = [
  {
    name: "B-INCOMESSF",
    index: 1
  },
  {
    name: "BM70SSF",
    index: 5
  },
  {
    name: "BEQSSF",
    index: 9
  },
  {
    name: "B-FUTURESSF",
    index: 13
  }
];

function fetchData(name) {
  console.log(name);
  const request = require("request");

  request(
    {
      headers: {
        accept: "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
        cookie: "hasCookie=true"
      },
      uri: "https://codequiz.azurewebsites.net",
      method: "GET"
    },
    function (err, res, body) {
      let test = body.split("<table>");
      let test2 = test[1].split("</table>");
      let test3 = test2[0].split("</th></tr>");

      // console.log(test3[1].split("</td><td>"));

      let nav = test3[1].split("</td><td>");

      // var regex = /[+-]?\d+(\.\d+)?/g;

      // var nav = test3[1].match(regex).map(function (v) {
      //   return parseFloat(v);
      // });

      let obj = fund.find((el) => el.name == name);
      console.log(nav[obj.index]);
    }
  );
}
